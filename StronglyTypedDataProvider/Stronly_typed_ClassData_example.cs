﻿using FluentAssertions;
using TestExamples.StronglyTypedDataProvider.SUT;
using Xunit;

namespace TestExamples.StronglyTypedDataProvider;

public class Stronly_typed_ClassData_example
{
    [Theory]
    [ClassData(typeof(TestDataProvider))]
    public void TheoryExample(Parcel p, Operator o)
    {

        p.Should().NotBeNull();
        o.Should().NotBeNull();

        p.Operator.Should().Be(o);
    }
}
