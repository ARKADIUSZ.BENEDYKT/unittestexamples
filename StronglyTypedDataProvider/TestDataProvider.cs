using TestExamples.StronglyTypedDataProvider.SUT;

namespace TestExamples.StronglyTypedDataProvider;

public class TestDataProvider : TheoryData<Parcel, Operator>
{
    public TestDataProvider()
    {
        var jan = new Operator("Jan");
        var marek = new Operator("Marek");

        Add(new Parcel(jan), jan);
        Add(new Parcel(marek), marek);
        Add(new Parcel("Stefan"), "Stefan");
        Add(new Parcel("Mariola"), "Mariola");

        //    Add(new Parcel(jan), marek);
    }
}
