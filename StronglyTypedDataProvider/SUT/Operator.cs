namespace TestExamples.StronglyTypedDataProvider.SUT;

public class Operator
{
    public string Name { get; }

    public Operator(string name)
    {
        Name = name;
    }

    public static implicit operator Operator(string name) => new Operator(name);

    // override object.Equals
    public override bool Equals(object? obj)
    {
        //
        // See the full list of guidelines at
        //   http://go.microsoft.com/fwlink/?LinkID=85237
        // and also the guidance for operator== at
        //   http://go.microsoft.com/fwlink/?LinkId=85238
        //

        if (obj == null || GetType() != obj.GetType())
        {
            return false;
        }

        // TODO: write your implementation of Equals() here
        return Name == ((Operator)obj).Name;
    }

    // override object.GetHashCode
    public override int GetHashCode()
    {
        return base.GetHashCode();
    }
}