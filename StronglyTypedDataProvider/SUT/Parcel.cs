using TestExamples.StronglyTypedDataProvider.SUT;

namespace TestExamples
{
    public class Parcel
    {
        public Operator Operator { get; }

        public Parcel(Operator @operator)
        { 
            Operator = @operator;
        }
    }
}