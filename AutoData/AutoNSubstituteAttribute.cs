using AutoFixture;
using AutoFixture.Xunit2;

namespace TestExamples.AutoData;

public class AutoNSubstituteAttribute: AutoDataAttribute
	{
		public AutoNSubstituteAttribute()
			: base(()=>new Fixture()
				.Customize(new AutoPopulatedNSubstitutePropertiesCustomization())
				)
		{
		}
	}
