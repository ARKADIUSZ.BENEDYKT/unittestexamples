namespace TestExamples.AutoData.SUT.Abstract;

public interface ISomeOtherDependency
{
    void Hello(ISomeArg argument);
}