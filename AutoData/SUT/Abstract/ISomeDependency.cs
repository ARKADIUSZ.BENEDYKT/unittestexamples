namespace TestExamples.AutoData.SUT.Abstract;

public interface ISomeDependency
{
    void Hello(ISomeArg exampleArgs);
}